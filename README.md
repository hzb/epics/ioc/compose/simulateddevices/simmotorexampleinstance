## Example Deployment of EPICs Motor Sim

To deploy this IOC in the background as is use

```
docker-compose up -d
``````

To stop the running container use 

```
docker-compose down
```

Or interact with the container directly. Using something like Portainer attached to the hosts docker daemon is one option. 

### Modifying the IOC

To change the PV Prefix change the values in the `.env` file

To add or remove motor channels or to change the default values for each motor, modify the `motorSim.cmd`,`motorSim.substitutions` and `settings.req` files. Be careful when editing `settings.req` to hit enter after the last line...

### Autosave

As is, this container will create autosave files in the directory `./autosave`. A subdirectory will be made automatically by docker-compose with the name $(IOC_SYS)_$(IOC_DEV) which is expected by the IOC. You could change the mount point if you like in the docker-compose file
